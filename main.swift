// Создадим массив
var array : [Int] = [3, 6, 1, 2, 11, 5, 9, 4, 12, 7, 10, 8];
print("Изначальный массив \(array)")
array = array.sorted( by : compAsc) // отсортируем по возрастанию
print("Отсортированный массив \(array)")
array = array.sorted(by : compDesc) // отсортируем по убыванию
print("В обратную сторону \(array)")

friends(names : "Маша", "Яша", "Борис");

arrayFunc(stringArray : ["a", "b"], intArray : [1, 2, 3]);
arrayFunc(stringArray : [], intArray : []);

func compDesc(i1 : Int, i2 : Int) -> Bool {i1 > i2} //Комопратор по убыванию
func compAsc(i1 : Int, i2 : Int) -> Bool {i2 > i1} //Комопратор по возрастанию

func friends(names : String...) // Метод который принимает имена друзей, сколько друзей мы не знаем поэтому используем "Вариативный" параметр
{
    func compLetter(s1 : String, s2 : String) -> Bool {s1.count < s2.count} // Комопратор для String по возрастанию
    // Создадим и заполним массив друзей
    var namesArray : [String] = []; 
    for i in names
    {
        namesArray.append(i);
    }
    print(namesArray);
    namesArray = namesArray.sorted( by : compLetter); // Отсортируем массив по возрастанию количества букв
    print(namesArray);
    // Создать словарь (Dictionary), где ключ - кол-во символов в имни, а в значении - имя друга
    // (если будут друзья с одинаковым количеством букв, то часть данных будет утеряна)
    var namesDictionary : Dictionary<Int, String> = [:];
    for i in names
    {
        namesDictionary.updateValue(i, forKey: i.count);
    }
    for key in namesDictionary.keys // Проверка функции getValue
    {
        getValue(key : key);
    }
    func getValue(key : Int) // Написать функцию, которая будет принимать ключ, выводить полученный ключ и значение
    {
        if namesDictionary[key] != nil // Ключ жолжен существовать
        {
            print("\(key) - \(namesDictionary[key]!)");
        }
    }
}

func arrayFunc(stringArray : [String], intArray : [Int]) // Написать функцию, которая принимает 2 массива (один строковый, второй - числовой)
{
    var stringArray = stringArray;
    var intArray = intArray;
    if stringArray.isEmpty // проверяет их на пустоту
    {
        stringArray.append("любое значения"); // если пустой - то добавьте любое значения и выведите массив в консоль
        print(stringArray);
    }
    if intArray.isEmpty // проверяет их на пустоту
    {
        intArray.append(Int.random(in: 1...1000)); // если пустой - то добавьте любое значения
    }
    print(intArray); // и выведите массив в консоль
}
